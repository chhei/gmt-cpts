# N.B.: THIS REPOSITORY HAS MOVED AND IS KEPT FOR ARCHIVAL PURPOSES ONLY

I have converted the GIT repos into a [Fossil](https://fossil-scm.org) repository which is now live at: 

[https://code.paleoearthlabs.org/geotimecpts](https://code.paleoearthlabs.org/geotimecpts)

A GIT-based downstream mirror is available on [Sourcehut](https://sr.ht) under this URL:

[https://git.sr.ht/~chhei/geotimecpts](https://git.sr.ht/~chhei/geotimecpts)

This repository here still contains the older commit history/files.

Christian Heine, 2021-09-05
